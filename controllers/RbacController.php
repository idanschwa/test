<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;


class RbacController extends Controller
{
	public function actionP()
	{
		$auth = Yii::$app->authManager;	
		
		$dealingWithDeals= $auth->createPermission('dealingWithDeals');
		$dealingWithDeals->description = 'Allows to create update delete deales';
		$auth->add($dealingWithDeals);
					
		
	}
	
	public function actionC()
	{
		$auth = Yii::$app->authManager;
		
		$teamleader = $auth->getRole('teamleader');
		
		$dealingWithDeals = $auth->getPermission('dealingWithDeals');
		$auth->addChild($teamleader, $dealingWithDeals);	
	}
	
	
	
}
